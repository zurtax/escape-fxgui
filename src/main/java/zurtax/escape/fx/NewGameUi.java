package zurtax.escape.fx;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.BorderPane;
import ru.nboo.newage.annotations.IntegerValueResource;
import ru.nboo.newage.annotations.StringResource;
import ru.nboo.newage.annotations.StringResourceContainer;
import ru.nboo.newage.annotations.ValueResourceContainer;
import ru.nboo.newage.core.JfxCore;
import ru.nboo.newage.core.Refreshable;

/**
 *
 * @author yura
 */
@StringResourceContainer(path = "escape", file = "newgame")
@ValueResourceContainer(prefix = "escape")
public class NewGameUi extends AbstractUi implements Initializable, Refreshable {

    private final static String UIFORM_NAME = "newgame.fxml";
    private final static String LAST_MAZE   = "last.maze";
    private final static String LAST_HP     = "last.hp";
    private final static String LAST_AMMO   = "last.ammo";
    private final static String LAST_IMPACT = "last.impact";
    private final static String LAST_SCRATCH= "last.scratch";
    private final static String LAST_POISON = "last.poison";
    private final static String LAST_HUNGERD= "last.hunger.damage";
    private final static String LAST_HUNGERP= "last.hunger.period";
    
    @StringResource(name = "label.maze")
    private static String labelMazeText;
    @StringResource(name = "label.ruleset")
    private static String labelRulesetText;
    @StringResource(name = "ruleset.standard")
    private static String labelRulesetStandard;
    @StringResource(name = "ruleset.custom")
    private static String labelRulesetCustom;
    @StringResource(name = "label.hitpoints")
    private static String labelHitpointsText;
    @StringResource(name = "label.ammo")
    private static String labelAmmoText;
    @StringResource(name = "label.impact")
    private static String labelImpactText;
    @StringResource(name = "label.scratch")
    private static String labelScratchText;
    @StringResource(name = "label.poison")
    private static String labelPoisonText;
    @StringResource(name = "label.hunger.damage")
    private static String labelHungerDamageText;
    @StringResource(name = "label.hunger.period")
    private static String labelHungerPeriodText;
    @StringResource(name = "button.start.game")
    private static String buttonStartGameText;
    
    @FXML private Label       labelMaze;
    @FXML private ChoiceBox   choiseMaze;
    @FXML private Label       labelRuleset;
    @FXML private RadioButton radioStandard;
    @FXML private RadioButton radioCustom;
    @FXML private Label   labelHitpoints;
    @FXML private Spinner spinnerHitpoints;
    @FXML private Label   labelAmmo;
    @FXML private Spinner spinnerAmmo;
    @FXML private Label   labelImpactDamage;
    @FXML private Spinner spinnerImpactDamage;
    @FXML private Label   labelScratchDamage;
    @FXML private Spinner spinnerScratchDamage;
    @FXML private Label   labelPoisonDamage;
    @FXML private Spinner spinnerPoisonDamage;
    @FXML private Label   labelHungerDamage;
    @FXML private Spinner spinnerHungerDamage;
    @FXML private Label   labelHungerPeriod;
    @FXML private Spinner spinnerHungerPeriod;
    @FXML private Button  buttonStart;
    
    
    @IntegerValueResource(id = LAST_MAZE, defaultValue = 1l)
    private Integer lastMaze;
    @IntegerValueResource(id = LAST_HP, defaultValue = 10l)
    private Integer lastHp;
    @IntegerValueResource(id = LAST_AMMO, defaultValue = 10l)
    private Integer lastAmmo;
    @IntegerValueResource(id = LAST_IMPACT, defaultValue = 1l)
    private Integer lastImpact;
    @IntegerValueResource(id = LAST_SCRATCH, defaultValue = 1l)
    private Integer lastScratch;
    @IntegerValueResource(id = LAST_POISON, defaultValue = 1l)
    private Integer lastPoison;
    @IntegerValueResource(id = LAST_HUNGERD, defaultValue = 1l)
    private Integer lastHungerDamage;
    @IntegerValueResource(id = LAST_HUNGERP, defaultValue = 10l)
    private Integer lastHungerPeriod;
    
    
    public NewGameUi (EscapeCore core) {
        super(core);
        init();
    }
    
    private void init() {
        core.getResourceManager().initFields(this);
    }
    
    private BorderPane panel;
    
    @Override
    public Node getPanel() {
        if (panel==null) {
            panel = newPanel(UIFORM_NAME);
        }
        return panel;
    }
    
    private void tune(Spinner spinner, int from, int till, int start) {
        spinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(from, till, start));
        spinner.getStyleClass().add(Spinner.STYLE_CLASS_ARROWS_ON_RIGHT_HORIZONTAL);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        labelMaze.setText(labelMazeText);
        labelRuleset.setText(labelRulesetText);
        radioStandard.setText(labelRulesetStandard);
        radioCustom.setText(labelRulesetCustom);
        labelHitpoints.setText(labelHitpointsText);
        tune(spinnerHitpoints, 1, 20, lastHp);
        labelAmmo.setText(labelAmmoText);
        tune(spinnerAmmo, 1, 20, lastAmmo);
        labelImpactDamage.setText(labelImpactText);
        tune(spinnerImpactDamage, 1, 10, lastImpact);
        labelScratchDamage.setText(labelScratchText);
        tune(spinnerScratchDamage, 1, 10, lastScratch);
        labelPoisonDamage.setText(labelPoisonText);
        tune(spinnerPoisonDamage, 1, 10, lastPoison);
        labelHungerDamage.setText(labelHungerDamageText);
        tune(spinnerHungerDamage, 1, 10, lastHungerDamage);
        labelHungerPeriod.setText(labelHungerPeriodText);
        tune(spinnerHungerPeriod, 1, 10, lastHungerPeriod);
        buttonStart.setText(buttonStartGameText);
        /*Начало: Тоже не самое хорошее решение. Жду ответа почему.*/
        List<String> list = core.getRestApi().getMazeList()
                .stream()
                .map(desc -> desc.MazeDesc)
                .collect(Collectors.toList());
        choiseMaze.getItems().addAll(list);
        choiseMaze.getSelectionModel().select(lastMaze.intValue());
        core.getRestApi().getMazeList();
    }

    public void startGame() {
        flushValues();
        boolean gameStarted = false;
        if (radioStandard.isSelected()) 
            gameStarted = core.getRestApi().start(Integer.toString(lastMaze+1));
        else if(radioCustom.isSelected())
            gameStarted = core.getRestApi().start(Integer.toString(lastMaze),
                    spinnerPoisonDamage.getEditor().getText(),
                    spinnerScratchDamage.getEditor().getText(),
                    spinnerImpactDamage.getEditor().getText(),
                    spinnerHungerDamage.getEditor().getText(),
                    spinnerHungerPeriod.getEditor().getText(),
                    spinnerHitpoints.getEditor().getText(),
                    spinnerAmmo.getEditor().getText() );
        if(gameStarted) {
            buttonStart.setDisable(true);
        }
    }
    
    private int store(Spinner spinner) {
        return Integer.parseInt(spinner.getValue().toString());
    }
    
    public void flushValues() {
        /*Продолжение: Тоже не самое хорошее решение. Жду ответа почему.*/
        lastMaze = choiseMaze.getSelectionModel().getSelectedIndex();
        lastHp = store(spinnerHitpoints);
        lastAmmo = store(spinnerAmmo);
        lastImpact = store(spinnerImpactDamage);
        lastScratch = store(spinnerScratchDamage);
        lastPoison = store(spinnerPoisonDamage);
        lastHungerDamage = store(spinnerHungerDamage);
        lastHungerPeriod = store(spinnerHungerPeriod);
    }

    @Override
    public void refresh() {
        buttonStart.setDisable(false);
    }
    
}
