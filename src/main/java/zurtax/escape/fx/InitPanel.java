package zurtax.escape.fx;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import static javafx.scene.layout.GridPane.setConstraints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nboo.newage.annotations.StringResource;
import ru.nboo.newage.annotations.StringResourceContainer;
import ru.nboo.newage.annotations.StringValueResource;
import ru.nboo.newage.annotations.ValueResourceContainer;
import ru.nboo.newage.core.Refreshable;

/**
 *
 * @author yura
 */
@StringResourceContainer(path = "escape", file = "init")
@ValueResourceContainer(prefix = "init")
public class InitPanel extends GridPane implements Refreshable {
    
    /** индентификаторы того, что мы ожидаем найтив конфигах **/
    private static final String INIT_LOGIN  = "connect.app_usernm";
    private static final String INIT_SRVURL = "connect.srv_url";
    private static final String LAST_LOGIN  = "last.login";
    private static final String LAST_SRVURL = "last.srvurl";
    
    /** Строки, которые мы прочтем из конфигов **/
    @StringResource(name = INIT_SRVURL)   
    private static String LABEL_SERVER_URL;
    @StringResource(name = INIT_LOGIN)   
    private static String LABEL_LOGIN_USER;

    /** Эти строки мы прочтем из конфига, кэширующего значения 
     * введеные пользователем при прошлых запусках **/
    @StringValueResource(id = LAST_LOGIN, defaultValue = "")
    private String apLoginStr;
    @StringValueResource(id = LAST_SRVURL, defaultValue = "")
    private String apServerUrl;
    
    private final Logger logger = LoggerFactory.getLogger(InitPanel.class);
    private final EscapeCore core;
    
    private TextField server;
    private TextField login;
    private Label callback;
    private StringProperty loggerCallback;
    
    
    public InitPanel(EscapeCore core) {
        this.core = core;
        init();
    }

    /**
     * Повторряющийся код вынесен в отдельный метод
     * @param field поле для "тонкой настройки"
     * @param prompt ТЕкст приглашения
     * @param value Значение по умолчанию
     * @return настроеный компонент
     */
    private TextField tune(TextField field, String prompt, String value) {
        field.setPromptText(prompt);
        field.setPrefColumnCount(10);
        if (!value.isEmpty())
            field.setText(value);
        return field;
    }
    
    /**
     * Здесь мы строим интерфейс панеьлки настройки подключения
     */
    private void init() {
        core.getResourceManager().initFields(this);
        this.setPadding(new Insets(10, 10, 10, 10));
        this.setVgap(5);
        this.setHgap(5);
        
        server = tune(new TextField(), LABEL_SERVER_URL, apServerUrl);
        setConstraints(server, 1,0);

        login = tune(new TextField(), LABEL_LOGIN_USER, apLoginStr);
        setConstraints(login, 1,1);

        callback = new Label("");
        setConstraints(callback, 1,2);
        
        this.getChildren().addAll(server, login, callback);
    }

    /**
     * Если пользователь изменил строку, пишем в конфиг
     * @return логин
     */
    public String getLogin() {
        if (!login.getText().equals(apLoginStr)) {
            apLoginStr = login.getText();
        }
        return apLoginStr;
    }

    /**
     * Если пользователь изменил строку, пишем в конфиг
     * @return адрес сервиса
     */
    public String getServer() {
        if (!server.getText().equals(apServerUrl)) {
            apServerUrl = server.getText();
        }
        return apServerUrl;
    }

    /**
     * Если при инициализации будут ошибки, через этот элемент 
     * мы их сообщения получим
     * @return 
     */
    public StringProperty getLoggerCallback() {
        if(loggerCallback==null) {
            loggerCallback= new SimpleStringProperty();
            core.join(()->callback.textProperty().bindBidirectional(loggerCallback));
        }
        return loggerCallback;
    }

    /**
     * Когда переключаемся на эту панельку, 
     * пусть она устанавливает фокус на
     * первое не заполненное поле
     */
    @Override
    public void refresh() {
        core.join(()->{
            if (server.getText().isEmpty()) {
                server.requestFocus();
            } else if (login.getText().isEmpty()) {
                login.requestFocus();
            }
        });
    }
    
}
