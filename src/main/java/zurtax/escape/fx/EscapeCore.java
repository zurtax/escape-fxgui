package zurtax.escape.fx;

import ru.nboo.newage.annotations.BooleanValueResource;
import ru.nboo.newage.annotations.StringValueResource;
import ru.nboo.newage.annotations.ValueResourceContainer;
import ru.nboo.newage.core.CoreConstants;
import ru.nboo.newage.core.JfxCore;

/**
 *
 * @author yura
 */
@ValueResourceContainer(prefix = "escape")
public class EscapeCore extends JfxCore {
    
    /* Эти строки нужны для того, чтобы обновления не искались */
    @BooleanValueResource(id = CoreConstants.ID_UPDATES_ENABLED, defaultValue = false)
    private static Boolean updEnabled;
    @StringValueResource(id = CoreConstants.ID_UPDATES_PROJECT, defaultValue = "")
    private static String dirRemote;
    
    /**
     * Это lazy создание нашего Rest Api
     */
    private RestApi restApi;
    public  RestApi getRestApi() {
        if(restApi==null) {
            restApi = new RestApi();
            restApi.init(this);
        }
        return restApi;
    }
    
}
