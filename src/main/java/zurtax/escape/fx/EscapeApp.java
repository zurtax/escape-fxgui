package zurtax.escape.fx;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.stage.Stage;
import ru.nboo.newage.core.AbstractModule;
import static javafx.application.Application.launch;


public class EscapeApp extends Application {

    /**
     * Запуск Java FX приложения начинается здесь
     * @param stage
     * @throws Exception 
     */
    @Override
    public void start(Stage stage) throws Exception {
        new App().runApp(stage, getParameters().getRaw().toArray(new String[]{}));
    }

    /**
     * Когда набираем java -jar this.jar оказываемся здесь.
     * Однако Java FX требует дополнительную инициализацию.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    /**
     * А здесь мы создаем экземпляр моего na-core.jar ксласса, 
     * реализующим инициализацию и старт.
     */
    private class App extends ru.nboo.newage.core.Application<Stage, Node, EscapeCore> {
        @Override public AbstractModule[] getModules() {
            /**
             * У нашей приложухи всего один режим работы
             */
            return new AbstractModule[]{ new EscapeModule() };
        }
    }

}
