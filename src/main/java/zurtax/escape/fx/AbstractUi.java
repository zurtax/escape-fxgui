package zurtax.escape.fx;

import java.net.URL;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import ru.nboo.newage.core.JfxCore;

/**
 *
 * @author yura
 */
public abstract class AbstractUi {
    protected final EscapeCore core;

    public AbstractUi(EscapeCore core) {
        this.core = core;
    }
    
    public abstract Node getPanel();
    
    protected <T extends Node> T newPanel(String formName) {
        T panel = null;
        try {
            URL formUrl = core
                    .getFileResources(JfxCore.DEFAULT_DIR_UIFORMS)
                    .getFileOrResource(formName);
            FXMLLoader loader = new FXMLLoader(formUrl);
            loader.setController(this);
            panel = loader.load();
        } catch (Exception ex) {
            core.getMainWindow().criticalError(formName, ex.getMessage(), ()->{});
        }
        return panel;
    }
}
