package zurtax.escape.fx;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import ru.nboo.newage.annotations.StringResource;
import ru.nboo.newage.annotations.StringResourceContainer;
import ru.nboo.newage.core.AbstractModule;
import ru.nboo.newage.core.CoreConstants;
import ru.nboo.newage.core.Refreshable;
import ru.nboo.newage.frame.InitWindow;

/**
 *
 * @author yura
 */
@StringResourceContainer(path = "escape", file = "init")
public class EscapeModule extends AbstractModule<Node, EscapeCore> {

    private InitPanel initPanel;

    private static final String ID_NEW_GAME = "new-game";
    private static final String ID_VIEWPORT = "viewport";


    @StringResource(name = "title.escape")
    private String title;
    @StringResource(name = "title."+ID_NEW_GAME)
    private String titleNewGame;
    @StringResource(name = "title."+ID_VIEWPORT)
    private String titleViewport;

    /**
     * Заголовок приложения, когда оно работает в этом режиме
     * @return строка, прочтеная из properties 
     * через StringResourceContainer/StringResource
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     * Панелька, где настраивается подключение.
     * Выбирается сервер и логин
     * @return 
     */
    @Override
    public Node getConfigPanel() {
        if (initPanel==null) {
            initPanel = new InitPanel(core);
        }
        return initPanel;
    }

    /**
     * UI пользователя древовидное. 
     * @return Панельки верхнего уровня
     */
    @Override
    public List<String> getRootInterfaces() {
        return Arrays.asList(ID_VIEWPORT, ID_NEW_GAME);
    }

    /**
     * У нас все панельки одного уровня, тпе что дочерних у них нет.
     * @param id
     * @return 
     */
    @Override
    public List<String> getChildrenFor(String id) {
        return Collections.EMPTY_LIST;
    }

    /**
     * Заголовки окон/панелек GUI
     * @param id для кого нужен заголовок
     * @return строка, прочтеная из properties 
     * через StringResourceContainer/StringResource
     */
    @Override
    public String getInterfaceTitle(String id) {
        switch(id) {
            case ID_VIEWPORT: return titleViewport;
            case ID_NEW_GAME: return titleNewGame;
        }
        return id;
    }

    /**
     * Кэш наших UI панелек/окошек
     */
    private final HashMap<String, AbstractUi> ui = new HashMap<>();
    
    /**
     * Создаваться панельки UI будут здесь
     * @param id какая панельлка нам нужна
     * @return готовая новая панелька
     */
    private AbstractUi generate(String id) {
        switch (id) {
            case ID_NEW_GAME: 
                return new NewGameUi(core);
            case ID_VIEWPORT: 
                return new ViewportUi(core);
        }
        return null;
    }

    /**
     * Это называется мемоизация, один раз вычисляется результат, 
     * потом берется из кэша.
     * @param id какой UI мы хотим получить
     * @return GUI панелька
     */
    @Override
    public Node getInterfaceComponent(String id) {
        ui.putIfAbsent(id, generate(id));
        Node node = ui.get(id)!=null
                ? ui.get(id).getPanel()
                : new Pane();
        return node;
    }

    /**
     * При переключении между панельками они могут блокировать доступ или
     * производить какие-то дополнительные действия при переключении на наи
     * или при уходе с них. Сейчас просто разрешаем любые переключения.
     * @param id
     * @param action
     * @return 
     */
    @Override
    public boolean aquireAction(String id, int action) {
        switch(action) {
            case CoreConstants.MODULE_ACTION_ACTIVATE:
                AbstractUi gui = ui.get(id);
                if(gui!=null && (gui instanceof Refreshable)) {
                    ((Refreshable)gui).refresh();
                }
            break;
            case CoreConstants.MODULE_ACTION_DEACTIVATE:
            case CoreConstants.MODULE_ACTION_TERMINATE:
                // ui.get(id).shutdown();
        }
        return true;
    }

    /**
     * Запуск приложения в этом режиме. Этот метод вызывается когда на экране
     * еще только панелька с вводом логина/пароля и прочего.
     * Вызов этого метода может осуществляться асинхронно, так что надо 
     * пользоваться им аккуратн. Результат подключения ожидается в callback.
     * @param callback ждет вызова его методов success или fail.
     */
    @Override
    public void start(InitWindow.InitCallback callback) {
        try {
            core.getRestApi().connect(initPanel.getLogin(), initPanel.getServer(), initPanel.getLoggerCallback());
            
            callback.success(this);
        } catch (Exception ex) {
            callback.fail(ex);
        }
    }
    
}
