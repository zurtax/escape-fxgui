package zurtax.escape.fx;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import ru.nboo.newage.annotations.ImageResource;
import ru.nboo.newage.annotations.ImageResourceContainer;
import ru.nboo.newage.annotations.IntegerValueResource;
import ru.nboo.newage.annotations.StringResource;
import ru.nboo.newage.annotations.StringResourceContainer;
import ru.nboo.newage.annotations.StringValueResource;
import ru.nboo.newage.annotations.ValueResourceContainer;
import ru.nboo.newage.core.Refreshable;

/**
 *
 * @author yura
 */
@ImageResourceContainer(path = "", type = "png", size = "50")
@StringResourceContainer(path = "escape", file = "viewport")
@ValueResourceContainer(prefix = "escape")
public class ViewportUi extends AbstractUi implements Initializable, Refreshable {

    private final static String UIFORM_NAME = "viewport.fxml";
    private static final String CELL_POSIT = "@"; 
    private static final String CELL_START = "°"; 
    private static final String CELL_TILE_ = " "; 
    private static final String CELL_EXIT_ = "√"; 
    private static final String CELL_FENSE = "╬"; 
    private static final String CELL_WALL_ = "█"; 
    private static final String CELL_CRATE = "■";
    private static final String CELL_STAIR = "^";
    private static final String CELL_FALL_ = "v";
    private static final String CELL_POISN = "▒";
    private static final String CELL_UNKNW = "░";
    private static final String DIR_NORTH  = "north";
    private static final String DIR_EAST   = "east";
    private static final String DIR_SOUTH  = "south";
    private static final String DIR_WEST   = "west";
    private static final String DIR_UP     = "up";
    private static final String DIR_DOWN   = "down";
    private static final int CELL_SIZE = 5;
    private final static PhongMaterial fog_mat = new PhongMaterial(Color.WHITESMOKE);
    private final static PhongMaterial wallmat = new PhongMaterial(Color.NAVY);
    private final static PhongMaterial falsmat = new PhongMaterial(Color.BLUE);
    private final static PhongMaterial fensmat = new PhongMaterial(Color.GREEN);
    private final static PhongMaterial poismat = new PhongMaterial(Color.CYAN);
    private final static PhongMaterial stirmat = new PhongMaterial(Color.SILVER);
    
    @StringResource(name = "label.hitpoints")
    private static String labelHitpointsText;
    @StringResource(name = "label.ammo")
    private static String labelAmmoText;

    @StringResource(name = "button.move")
    private static String buttonMoveText;
    @StringResource(name = "button.left")
    private static String buttonLeftText;
    @StringResource(name = "button.right")
    private static String buttonRightText;
    @StringResource(name = "button.up")
    private static String buttonUpText;
    @StringResource(name = "button.down")
    private static String buttonDownText;
    @StringResource(name = "button.throw")
    private static String buttonThrowText;

    @ImageResource(name = "move")
    private URL imageMove;
    @ImageResource(name = "left")
    private URL imageLeft;
    @ImageResource(name = "right")
    private URL imageRight;
    @ImageResource(name = "up")
    private URL imageUp;
    @ImageResource(name = "down")
    private URL imageDown;
    @ImageResource(name = "throw")
    private URL imageThrow;
    
    @IntegerValueResource(id = "font.size", defaultValue = 12)
    private Integer fontSize;
    @StringValueResource(id = "font.name", defaultValue = "Courier New")
    private String fontName;
    
    @FXML private Label         labelAzimuth;
    @FXML private TextArea      textTimeline;
    @FXML private TextArea      textAutomap;
    @FXML private Label         labelHitpoints;
    @FXML private TextField     textHitpoints;
    @FXML private Label         labelAmmo;
    @FXML private TextField     textAmmo;
    @FXML private Button        buttonLeft;
    @FXML private Button        buttonMove;
    @FXML private Button        buttonRight;
    @FXML private Button        buttonDown;
    @FXML private Button        buttonUp;
    @FXML private Button        buttonThrow;
    @FXML private BorderPane    panelViewport;
    
    private String direction = "north";
    private int currentX = 0;
    private int currentY = 0;
    private int currentZ = 0;
    
    public ViewportUi (EscapeCore core) {
        super(core);
        init();
    }
    
    private void init() {
        core.getResourceManager().initFields(this);
    }
    
    private BorderPane panel;
        
    @Override
    public Node getPanel() {
        if (panel==null) {
            panel = newPanel(UIFORM_NAME);
        }
        return panel;
    }
    
    private void setup(Button button, URL url, String alter) {
        if (url!=null) {
            Image image = new Image(url.toExternalForm(), 50, 50, true, true, true);
            ImageView imageView = new ImageView(image);
            button.setText("");
            button.setGraphic(imageView);
        } else {
            button.setText(alter);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        labelHitpoints.setText(labelHitpointsText);
        labelAmmo.setText(labelAmmoText);
        setup(buttonDown,  imageDown,  buttonDownText);
        setup(buttonLeft,  imageLeft,  buttonLeftText);
        setup(buttonMove,  imageMove,  buttonMoveText);
        setup(buttonRight, imageRight, buttonRightText);
        setup(buttonUp,    imageUp,    buttonUpText);
        setup(buttonThrow, imageThrow, buttonThrowText);
        textAutomap.setFont(new Font(fontName, fontSize));
        labelAzimuth.setFont(new Font(fontName, fontSize*3));
        refresh();
    }

    @Override
    public void refresh() {
        textAutomap.setText(core.getRestApi().getMap());
        textTimeline.setText(core.getRestApi().getLog());
        RestApi.Status status = core.getRestApi().getStatus();
        textHitpoints.setText(status.getHp());
        textAmmo.setText(status.getNuts());
        currentX = status.getX();
        currentY = status.getY();
        currentZ = status.getZ();
        drawScene();
    }

    public void right() {
        switch (direction) {
            case DIR_NORTH: direction = DIR_EAST;  break;
            case DIR_EAST : direction = DIR_SOUTH; break;
            case DIR_SOUTH: direction = DIR_WEST;  break;
            case DIR_WEST : direction = DIR_NORTH; break;
        }
        labelAzimuth.setText(direction.substring(0, 1).toUpperCase());
        drawScene();
    }

    public void left() {
        switch (direction) {
            case DIR_NORTH: direction = DIR_WEST;  break;
            case DIR_WEST:  direction = DIR_SOUTH; break;
            case DIR_SOUTH: direction = DIR_EAST;  break;
            case DIR_EAST:  direction = DIR_NORTH; break;
        }
        labelAzimuth.setText(direction.substring(0, 1).toUpperCase());
        drawScene();
    }

    public void climb() {
        core.getRestApi().move(DIR_UP);
        refresh();
    }

    public void jump() {
        core.getRestApi().move(DIR_DOWN);
        refresh();
    }

    public void throwNut() {
        core.getRestApi().check(direction);
        refresh();
    }

    public void makeStep() {
        core.getRestApi().move(direction);
        refresh();
    }
    
    private List<Cell> parse(String[] source) {
        ArrayList<Cell> list = new ArrayList<>();
        int radius = (source.length-1)/2;
        int y = radius;
        for(String string: source) {
            for(int i=0; i<string.length(); i++) {
                list.add(new Cell(i-radius, y, string.substring(i, i+1)));
            }
            y--;
        }
        return list;
    }
    
    private void drawScene() {
        List<Cell> all = parse(textAutomap.getText().split("\n"));
        
        PerspectiveCamera camera = newCamera(DIR_NORTH);
        Group root = new Group();       
        root.getChildren().add(camera);
        root.getChildren().addAll( witoutBehind(all) );
        
        SubScene subScene = new SubScene(
                root, 
                panelViewport.getPrefWidth()*2, 
                panelViewport.getPrefHeight()*2, 
                true, 
                SceneAntialiasing.BALANCED);
        subScene.setFill(Color.TRANSPARENT);
        subScene.setCamera(camera);
        
        panelViewport.setCenter(new Group(subScene));
    }
    
    private class Cell { 
        final int x;
        final int y;
        final String value; 
        public Cell(int x, int y, String value) { this.x = x; this.y = y; this.value = value; }
    }
    
    private boolean isVisible(Cell cell) {
        switch (direction) {
            case DIR_NORTH: return cell.y >= currentY;
            case DIR_WEST:  return cell.x <= currentX;
            case DIR_SOUTH: return cell.y <= currentY;
            case DIR_EAST:  return cell.x >= currentX;
            default: return false;
        }
    }
    
    private Cell translatePlan(Cell cell) {
        return new Cell(cell.x-currentX, cell.y+currentY, cell.value);
    }
    
    private Cell rotatePlan(Cell cell) {
        switch (direction) {
            case DIR_WEST:  return new Cell( cell.y, -cell.x, cell.value);
            case DIR_SOUTH: return new Cell(-cell.x, -cell.y, cell.value);
            case DIR_EAST:  return new Cell(-cell.y,  cell.x, cell.value);
            default: return cell;
        }
    }
    
    private Box toBox(Cell cell) {
        switch (cell.value) {
            case CELL_EXIT_:
            case CELL_POSIT:
            case CELL_START:
            case CELL_TILE_:
                return genTile(cell.x, cell.y);
            case CELL_CRATE:
                return genFalseWall(cell.x, cell.y);
            case CELL_POISN:
                return genPoison(cell.x, cell.y);
            case CELL_UNKNW:
                return genFog(cell.x, cell.y);
            case CELL_WALL_:
                return genWall(cell.x, cell.y);
            case CELL_STAIR:
                return genStairDown(cell.x, cell.y);
            case CELL_FALL_:
                return genStairUp(cell.x, cell.y);
            case CELL_FENSE:
            default:
                return genFense(cell.x, cell.y);
        }
    }
    
    private List<Box> witoutBehind(List<Cell> all) {
        return all.stream()
                //.filter(cell -> isVisible(cell))
                .map(cell -> translatePlan(cell))
                .map(cell -> rotatePlan(cell))
                .map(cell -> toBox(cell))
                .collect(Collectors.toList());
    }

    private PerspectiveCamera newCamera(String dir) {
        PerspectiveCamera camera = new PerspectiveCamera(true);
        switch (dir) {
            case DIR_NORTH: 
                camera.getTransforms().addAll(
                    new Translate(0, 0, -CELL_SIZE*1.5)); 
                break;
            case DIR_EAST:
                camera.getTransforms().addAll(
                        new Translate(-CELL_SIZE*1.5, 0, 0),
                        new Rotate(-90, Rotate.Y_AXIS)); 
                break;
            case DIR_SOUTH:
                camera.getTransforms().addAll(
                        new Translate(0, 0, CELL_SIZE*1.5),
                        new Rotate(180, Rotate.Y_AXIS)); 
                break;
            case DIR_WEST:
                camera.getTransforms().addAll(
                        new Translate(CELL_SIZE*1.5, 0, 0),
                        new Rotate(90, Rotate.Y_AXIS)); 
                break;
        }
        return camera;
    }

    private Box genFog(int x, int y) {
        Box fog = new Box(CELL_SIZE, CELL_SIZE, CELL_SIZE);
        fog.setMaterial(fog_mat);
        fog.translateXProperty().set(x*CELL_SIZE);
        fog.translateZProperty().set(y*CELL_SIZE);
        return fog;
    }
    
    private Box genFense(int x, int y) {
        Box wall = new Box(CELL_SIZE, CELL_SIZE, CELL_SIZE);
        wall.setMaterial(fensmat);
        wall.translateXProperty().set(x*CELL_SIZE);
        wall.translateZProperty().set(y*CELL_SIZE);
        return wall;
    }
    
    private Box genWall(int x, int y) {
        Box wall = new Box(CELL_SIZE, CELL_SIZE, CELL_SIZE);
        wall.setMaterial(wallmat);
        wall.translateXProperty().set(x*CELL_SIZE);
        wall.translateZProperty().set(y*CELL_SIZE);
        return wall;
    }
    
    private Box genFalseWall(int x, int y) {
        Box wall = new Box(CELL_SIZE, CELL_SIZE, CELL_SIZE);
        wall.setMaterial(falsmat);
        wall.translateXProperty().set(x*CELL_SIZE);
        wall.translateZProperty().set(y*CELL_SIZE);
        return wall;
    }
    
    private Box genTile(int x, int y) {
        Box tile = new Box(CELL_SIZE, 1, CELL_SIZE);
        tile.setMaterial(fog_mat);
        tile.translateXProperty().set(x*CELL_SIZE);
        tile.translateYProperty().set((CELL_SIZE-1)/2.0);
        tile.translateZProperty().set(y*CELL_SIZE);
        return tile;
    }
    
    private Box genPoison(int x, int y) {
        Box tile = new Box(CELL_SIZE, 1, CELL_SIZE);
        tile.setMaterial(poismat);
        tile.translateXProperty().set(x*CELL_SIZE);
        tile.translateYProperty().set((CELL_SIZE-1)/2.0);
        tile.translateZProperty().set(y*CELL_SIZE);
        return tile;
    }
    
    private Box genStairDown(int x, int y) {
        Box tile = new Box(CELL_SIZE, 1, CELL_SIZE);
        tile.setMaterial(stirmat);
        tile.translateXProperty().set(x*CELL_SIZE);
        tile.translateYProperty().set((CELL_SIZE-1)/2.0);
        tile.translateZProperty().set(y*CELL_SIZE);
        return tile;
    }
    private Box genStairUp(int x, int y) {
        Box tile = new Box(CELL_SIZE, 1, CELL_SIZE);
        tile.setMaterial(stirmat);
        tile.translateXProperty().set(x*CELL_SIZE);
        tile.translateYProperty().set((CELL_SIZE+1)/-2.0);
        tile.translateZProperty().set(y*CELL_SIZE);
        return tile;
    }
    
}
