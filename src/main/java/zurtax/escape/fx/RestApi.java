package zurtax.escape.fx;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.StringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nboo.newage.annotations.StringResourceContainer;
import ru.nboo.newage.annotations.StringValueResource;
import ru.nboo.newage.annotations.ValueResourceContainer;

/**
 *
 * @author yura
 */
@ValueResourceContainer(prefix = "escape")
@StringResourceContainer(path = "escape", file = "init")
public class RestApi {
    
    private EscapeCore core = null;
    private final Logger logger = LoggerFactory.getLogger(RestApi.class);
    
    @StringValueResource(id = "swagger.name", defaultValue = "/swagger.json")
    private String swaggerName;
    @StringValueResource(id = "api.prefix", defaultValue = "/api")
    private String apiPrefix;
    @StringValueResource(id = "maze.list", defaultValue = "/maze/all")
    private String mazeList;
    @StringValueResource(id = "player.endpoint", defaultValue = "/player/")
    private String playerEndpoint;
    
    public void init(EscapeCore core) {
        this.core = core;
        core.getResourceManager().initFields(this);
    }

    public RestApi() {
        
    }
    
    private String service = null;
    private String player = null;
    
    /**
     * Здесь будет проверка доступности сервера, и подключение к игре, 
     * если она есть.
     * @param login
     * @param service
     * @param callback
     * @throws IllegalStateException 
     */
    public void connect(String login, String service, StringProperty callback) 
            throws IllegalStateException {
        String swagger = get(service + swaggerName);
        if(swagger!=null && !swagger.isEmpty()) {
            this.service = service+apiPrefix;
            this.player = login;
        } else {
            this.service = null;
            this.player = null;
            throw new IllegalStateException(service + swaggerName);
        }
    }
    
    public class MazeDesc {
        public MazeDesc(int mazeNo, String MazeDesc) {
            this.mazeNo = mazeNo; this.MazeDesc = MazeDesc;
        }
        public final int mazeNo;
        public final String MazeDesc;
    }
    
    public class Status {
        private String hp = "0";
        private String nuts = "0";
        private int x =0;
        private int y =0;
        private int z =0;
        public void setX(int x) { this.x = x; }
        public  int getX() { return x; }
        public void setY(int y) { this.y = y; }
        public  int getY() { return y; }
        public void setZ(int z) { this.z = z; }
        public  int getZ() { return z; }
        public String getHp() { return hp; }
        public String getNuts() { return nuts; }
        public void setHp(String v) { hp=v; }
        public void setNuts(String v) { nuts=v; }
    }
    
    private class Seq {
        public Seq(MazeDesc head, String tail) {
            this.head = head;
            this.tail = tail;
        }
        public final MazeDesc head;
        public final String tail;
    }
    
    public Status getStatus() {
        StringBuilder request = new StringBuilder();
        request.append(service)
                .append(playerEndpoint).append(player);
        String response = get(request.toString());
        Status status = new Status();
        int start, stop;
        start = response.indexOf("\"hp\":");
        stop = response.indexOf(",", start);
        status.setHp(response.substring(start+5, stop));
        start = response.indexOf("\"shots\":", start);
        stop = response.indexOf(",", start);
        status.setNuts(response.substring(start+8, stop));
        start = response.indexOf("\"coords\":", start);
        try {
            start = response.indexOf("\"x\":", start);
            stop = response.indexOf(",", start);
            status.setX(Integer.parseInt(response.substring(start+4, stop)));
            start = response.indexOf("\"y\":", start);
            stop = response.indexOf(",", start);
            status.setY(Integer.parseInt(response.substring(start+4, stop)));
            start = response.indexOf("\"z\":", start);
            stop = response.indexOf(",", start);
            status.setZ(Integer.parseInt(response.substring(start+4, stop)));
        } catch (NumberFormatException ex ) {
            logger.error("status parsing failed: {}", ex.getMessage());
        }
        return status;
    }
    
    public String getMap() {
        StringBuilder request = new StringBuilder();
        request.append(service)
                .append(playerEndpoint).append(player)
                .append("/automap");
        return get(request.toString());
    }
    
    public String getLog() {
        StringBuilder request = new StringBuilder();
        request.append(service)
                .append(playerEndpoint).append(player)
                .append("/timeline?sort=desc");
        return get(request.toString()).replaceAll("\",\"", "\",\n\"");
    }
    
    public void check(String direction) {
        StringBuilder request = new StringBuilder();
        request.append(service)
                .append(playerEndpoint).append(player)
                .append("/check?direction=").append(direction);
        put(request.toString());
    }

    public void move(String direction) {
        StringBuilder request = new StringBuilder();
        request.append(service)
                .append(playerEndpoint).append(player)
                .append("/move?direction=").append(direction);
        put(request.toString());
    }
    
    /**
     * Отвратительный алгоритм. Подумай почему, сообщи мне свое мнение.
     * @param string
     * @return 
     */
    private Seq next(String string) {
        int numberIdx = string.indexOf("mazeNo");
        int descrIdx  = string.indexOf("mazeDesc");
        int stopIdx   = string.indexOf("}");
        if (numberIdx>0 && descrIdx>numberIdx && stopIdx>descrIdx) {
            String str = string.substring(numberIdx+8, descrIdx-2);
            int number = Integer.parseInt(str);
            String descr = string.substring(descrIdx+11, stopIdx-1);
            return new Seq(new MazeDesc(number, descr), string.substring(stopIdx+2));
        } else {
            return null;
        }
    }
    
    public List<MazeDesc> getMazeList() {
        String json = get(service + mazeList);
        if (json==null||json.isEmpty())
            return Collections.EMPTY_LIST;
        ArrayList<MazeDesc> res = new ArrayList<>();
        /* см. описание метода next выше.*/
        Seq sequence = next(json);
        while(sequence!=null) {
            res.add(sequence.head);
            sequence = next(sequence.tail);
        }
        return res;
    }
    
    public boolean start(String maze) {
        StringBuilder request = new StringBuilder();
        request.append(service)
                .append(playerEndpoint).append(player)
                .append("?mazeNo=").append(maze);
        String resp = post(request.toString());
        // Лучше сравнивать кодв ответа
        return resp!=null && !resp.isEmpty() && resp.startsWith("{\"id\":");
    }

    public boolean start(String maze, String poison, String crate, String wall, String hangdmg, String hangtics, String starthp, String startnuts) {
        StringBuilder request = new StringBuilder();
        request.append(service)
                .append(playerEndpoint).append(player)
                .append("/custom?mazeNo=").append(maze)
                .append("&wallDamage=").append(wall)
                .append("&crateDamage=").append(crate)
                .append("&poisonDamage=").append(poison)
                .append("&hungerDamage=").append(hangdmg)
                .append("&hungerPeriod=").append(hangtics)
                .append("&startingHealth=").append(starthp)
                .append("&startingShots=").append(startnuts);
        String resp = post(request.toString());
        // Лучше сравнивать кодв ответа
        return resp!=null && !resp.isEmpty() && resp.startsWith("{\"id\":");
    }
    
    //--------------------------------------------------------------------------
    
    private String get(String request) {
        StringBuilder content = new StringBuilder();
        try {
            logger.debug("RestApi: {}", request);
            URL url = new URL(request);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(false);
            httpCon.setRequestMethod("GET");
            logger.debug("RestApi: GET ({}) {}", httpCon.getResponseCode(), request);
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpCon.getInputStream(), "UTF-8"))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line).append("\n");
                }
            }
        } catch (Exception ex) {
            logger.error("RestApi: {}", ex.getMessage());
        }
        return content.toString();
    }
    
    public String put(String request){
        StringBuilder content = new StringBuilder();
        try {
            logger.debug("RestApi: {}", request);
            URL url = new URL(request);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            out.close();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpCon.getInputStream(), "UTF-8"))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line).append("\n");
                }
            }
        } catch (Exception ex) {
            logger.error("RestApi: {}", ex.getMessage());
        }
        return content.toString();
    }
    
    public String post(String request){
        StringBuilder content = new StringBuilder();
        try {
            logger.debug("RestApi: {}", request);
            URL url = new URL(request);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            out.close();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpCon.getInputStream(), "UTF-8"))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line).append("\n");
                }
            }
        } catch (Exception ex) {
            logger.error("RestApi: {}", ex.getMessage());
        }
        return content.toString();
    }

}
